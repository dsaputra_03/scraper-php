<?php
class MainController
{
    public function getAllUsers()
    {
        // Ambil semua pengguna dari database atau sumber data lainnya
        $users = array(
            array('id' => 1, 'name' => 'John'),
            array('id' => 2, 'name' => 'Jane')
        );

        header('Content-Type: application/json');
        echo json_encode($users);
    }

    public function scrapeTokopedia($code)
    {
        $pythonScript = escapeshellcmd('../scraping.py');
        // $code = "nmga3seWUCb";
        // Menjalankan skrip Python dan menyimpan hasil keluarannya
        $output = shell_exec("python3 $pythonScript https://tokopedia.link/$code");

        // Menampilkan hasil keluaran skrip Python
        echo $output;
    }

    // Implementasikan metode lain seperti addUser, updateUser, dan deleteUser
}