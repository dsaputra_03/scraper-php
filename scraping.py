from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import json
import argparse
import requests

def scrape_tokopedia(link):
    url = f'{link}'

    try:
        driver = webdriver.Chrome()
        driver.get(url)

        soup = BeautifulSoup(driver.page_source, 'html.parser')
        title = soup.find('h1').text
        id_product = title.split()
        stock = soup.find(attrs={"data-testid": "stock-label"})
        stock = stock.find('b').text
        final_stock = [int(s) for s in stock.split() if s.isdigit()]
        
        data = {
                    'id': id_product[-1],
                    'title': title,
                    'harga': soup.find('div', class_=('price')).text,
                    'stock': final_stock
                }
        return data
       
    except requests.exceptions.RequestException as e:
        print("Error:", e)
        return None

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Web scraping script with URL parameter.')
    parser.add_argument('url', type=str, help='URL of the website to scrape')
    args = parser.parse_args()

    url_to_scrape = args.url
    result = scrape_tokopedia(url_to_scrape)
    
    print(json.dumps(result))